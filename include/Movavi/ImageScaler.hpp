#pragma once

namespace Movavi {

class ImageScaler {
  QImage mImage;

 public:
  explicit ImageScaler(const QImage& image);

  void Downscale(float ratio);
  const QImage& GetImage() const;
};

}  // namespace Movavi