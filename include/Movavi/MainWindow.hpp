#pragma once
#include <Movavi/ImageData.hpp>

namespace Movavi {

class MainWindow : public QWidget {
  Q_OBJECT

  // Layout
  const std::unique_ptr<QVBoxLayout> mLayoutMain = std::make_unique<QVBoxLayout>(this);
  const std::unique_ptr<QHBoxLayout> mLayoutMenuPart = std::make_unique<QHBoxLayout>();
  const std::unique_ptr<QPushButton> mBtnClear = std::make_unique<QPushButton>();
  const std::unique_ptr<QPushButton> mBtnChooseFile = std::make_unique<QPushButton>();
  const std::unique_ptr<QComboBox> mComboImage = std::make_unique<QComboBox>();
  const std::unique_ptr<QLabel> mLabelLayer = std::make_unique<QLabel>();
  const std::unique_ptr<QComboBox> mComboLayer = std::make_unique<QComboBox>();
  const std::unique_ptr<QLabel> mLabelSize = std::make_unique<QLabel>();
  const std::unique_ptr<QLabel> mLabelDownscaleRatio = std::make_unique<QLabel>();
  const std::unique_ptr<QLineEdit> mInputDownscaleRatio = std::make_unique<QLineEdit>();
  const std::unique_ptr<QPushButton> mBtnDownscaleRatio = std::make_unique<QPushButton>();
  const std::unique_ptr<QVBoxLayout> mLayoutImagePart = std::make_unique<QVBoxLayout>();
  const std::unique_ptr<QGraphicsView> mImageView = std::make_unique<QGraphicsView>();

  // Internals
  std::vector<ImageData> mImageDatas;
  std::unique_ptr<QGraphicsScene> mGraphicsScene = std::make_unique<QGraphicsScene>();
  ImageData* mCurrentImageData;

 public:
  explicit MainWindow(QWidget* parent = {});
  virtual ~MainWindow();

 private slots:
  void OnClearBtn_Click();
  void OnChooseFileBtn_Click();
  void OnDownscaleRatioBtn_Click();
  void OnFileCombo_Activated(int index);
  void OnLayerCombo_Activated(int index);

 private:
  void SetCurrentImageData(ImageData& imageData);

  void UploadImage(const char* const filePath);
  void ShowImage(const ImageData& imageData, unsigned int level = 0);
  void ShowImage(const QImage& image, float level);
  void UpdateImageComboBox(const ImageData& imageData);
  void UpdateLayerComboBox(const ImageData& imageData);
  void UpdateSizeLabel(const QImage& image);

  void ClearGraphicsScene();
  void RecreateGraphicsScene();
};

}  // namespace Movavi
