#pragma once

namespace Movavi {

class ImageData final {
  std::vector<std::shared_ptr<QGraphicsPixmapItem>> mGraphicsItems;
  std::shared_ptr<QGraphicsPixmapItem> mBaseGraphicsItem;
  unsigned int mMaxDownscaleCount;
  QImage mBaseImage;

 public:
  const QUuid Uuid;
  const QFileInfo FileInfo;

 public:
  ImageData(const QFileInfo& fileInfo);
  ImageData(ImageData&& other) noexcept;

  std::shared_ptr<QGraphicsPixmapItem> GetGraphicsItem(unsigned int level) const;
  std::shared_ptr<QGraphicsPixmapItem> CreateDownscaleImage(unsigned int level);
  unsigned int GetMaxDownscaleCount() const;
  const QImage& GetBaseImage() const;

  bool operator==(const ImageData& other) const;
  bool operator!=(const ImageData& other) const;
};

}  // namespace Movavi