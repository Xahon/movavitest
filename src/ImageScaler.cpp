#include <Movavi/ImageScaler.hpp>

namespace Movavi {

ImageScaler::ImageScaler(const QImage& image)
    : mImage(image) {
}

void ImageScaler::Downscale(float ratio) {
  const QImage& oldImage = mImage;
  int oldWidth = oldImage.width();
  int oldHeight = oldImage.height();

  int newWidth = oldWidth / ratio;
  int newHeight = oldHeight / ratio;
  QImage newImage(newWidth, newHeight, QImage::Format::Format_RGBA64);

  for (int y = 0; y < newHeight; ++y) {
    for (int x = 0; x < newWidth; ++x) {
      float xProg = float(x) / newWidth;
      float yProg = float(y) / newHeight;
      int xPos = int(xProg * oldWidth);
      int yPos = int(yProg * oldHeight);
      newImage.setPixelColor(x, y, oldImage.pixelColor(xPos, yPos));
    }
  }
  mImage = newImage;
}

const QImage& ImageScaler::GetImage() const {
  return mImage;
}

}