#include <Movavi/ImageScaler.hpp>
#include <Movavi/MainWindow.hpp>

namespace Movavi {

MainWindow::MainWindow(QWidget *parent)
    : QWidget(parent) {
  mBtnClear->setText("Clear");
  mBtnChooseFile->setText("File ...");
  mLabelLayer->setText("Layer: ");
  mLabelSize->setText("Size: 0x0");
  mLabelDownscaleRatio->setText("Downscale ratio: ");
  mBtnDownscaleRatio->setText("Scale");
  mInputDownscaleRatio->setValidator(new QRegExpValidator(QRegExp("[-+]?[0-9]*\\.?[0-9]+")));

  mLayoutMenuPart->addWidget(mBtnClear.get());
  mLayoutMenuPart->addWidget(mBtnChooseFile.get());
  mLayoutMenuPart->addWidget(mComboImage.get());
  mLayoutMenuPart->addWidget(mLabelLayer.get());
  mLayoutMenuPart->addWidget(mComboLayer.get());
  mLayoutMenuPart->addWidget(mLabelSize.get());
  mLayoutMenuPart->addWidget(mLabelDownscaleRatio.get());
  mLayoutMenuPart->addWidget(mInputDownscaleRatio.get());
  mLayoutMenuPart->addWidget(mBtnDownscaleRatio.get());

  mLayoutImagePart->addWidget(mImageView.get());
  mImageView->setScene(mGraphicsScene.get());

  mLayoutMain->addLayout(mLayoutMenuPart.get());
  mLayoutMain->addLayout(mLayoutImagePart.get());

  connect(mBtnClear.get(), SIGNAL(clicked(bool)), this, SLOT(OnClearBtn_Click()));
  connect(mBtnChooseFile.get(), SIGNAL(clicked(bool)), this, SLOT(OnChooseFileBtn_Click()));
  connect(mBtnDownscaleRatio.get(), SIGNAL(clicked(bool)), this, SLOT(OnDownscaleRatioBtn_Click()));
  connect(mComboImage.get(), SIGNAL(activated(int)), this, SLOT(OnFileCombo_Activated(int)));
  connect(mComboLayer.get(), SIGNAL(activated(int)), this, SLOT(OnLayerCombo_Activated(int)));
}

MainWindow::~MainWindow() {
  ClearGraphicsScene();  // let ImageData class remove its own members itself
}

void MainWindow::OnClearBtn_Click() {
  RecreateGraphicsScene();
}

void MainWindow::OnDownscaleRatioBtn_Click() {
  if (!mCurrentImageData) {
    return;
  }

  float ratio{};
  try {
    ratio = std::stof(mInputDownscaleRatio->text().toUtf8().constData());
  } catch (...) {
    return;
  }

  ImageScaler scaler(mCurrentImageData->GetBaseImage());
  scaler.Downscale(ratio);
  ShowImage(scaler.GetImage(), ratio);
}

void MainWindow::OnChooseFileBtn_Click() {
  const QString picturesPath = QStandardPaths::locate(QStandardPaths::PicturesLocation, "", QStandardPaths::LocateOption::LocateDirectory);
  const QString filePath = QFileDialog::getOpenFileName(this, "", picturesPath);

  if (!filePath.isEmpty()) {
    UploadImage(filePath.toUtf8().constData());
  }
};

void MainWindow::OnFileCombo_Activated(int index) {
  if (index >= mComboImage->count() || index < 0) {
    return;
  }
  const QVariant variant = mComboImage->itemData(index);
  const QUuid uuid = variant.toUuid();

  const auto uuidsEqual = [&uuid](const ImageData &d) { return d.Uuid == uuid; };
  const auto imageDataIt = std::find_if(mImageDatas.begin(), mImageDatas.end(), uuidsEqual);
  if (imageDataIt != mImageDatas.end()) {
    SetCurrentImageData(*imageDataIt);
    ShowImage(*imageDataIt);
  }
}

void MainWindow::OnLayerCombo_Activated(int index) {
  if (index >= mComboLayer->count() || index < 0 || !mCurrentImageData) {
    return;
  }

  unsigned int level = index;
  if (!mCurrentImageData->GetGraphicsItem(level)) {
    mCurrentImageData->CreateDownscaleImage(level);
  }
  ShowImage(*mCurrentImageData, index);
}

void MainWindow::SetCurrentImageData(ImageData &imageData) {
  mCurrentImageData = &imageData;
}

void MainWindow::UploadImage(const char *const filePath) {
  const QImage image(filePath);
  if (image.isNull()) {
    QString mes = "Cannot open file: ";
    mes.append(filePath);
    mes.append("\nFile is not accessible to read or not an image file");
    QMessageBox::critical(this, "Error", mes);
    return;
  }

  const auto filePathEqual = [&filePath](const ImageData &d) { return d.FileInfo.filePath() == filePath; };
  const auto dupeIt = std::find_if(mImageDatas.begin(), mImageDatas.end(), filePathEqual);
  if (dupeIt != mImageDatas.end()) {
    ShowImage(*dupeIt);
    return;
  }

  mImageDatas.emplace_back(ImageData(QFileInfo(filePath)));
  ImageData &imageData = mImageDatas.back();

  SetCurrentImageData(imageData);
  mComboImage->addItem(imageData.FileInfo.baseName(), QVariant(imageData.Uuid));
  mComboImage->setCurrentIndex(mComboImage->count() - 1);

  ShowImage(imageData);
}

void MainWindow::ShowImage(const ImageData &imageData, unsigned int level) {
  RecreateGraphicsScene();

  auto graphicsItem = imageData.GetGraphicsItem(level);
  mGraphicsScene->addItem(graphicsItem.get());

  UpdateImageComboBox(imageData);
  UpdateLayerComboBox(imageData);
  mComboLayer->setCurrentIndex(level);
  UpdateSizeLabel(graphicsItem->pixmap().toImage());

  auto pixmapSize = graphicsItem->pixmap().size();
  auto origSize = imageData.GetBaseImage().size();
  auto diffSize = QSizeF(origSize.width() / pixmapSize.width(), origSize.height() / pixmapSize.height());
  graphicsItem->setTransform(QTransform::fromScale(diffSize.width(), diffSize.height()));
}

void MainWindow::ShowImage(const QImage &image, float ratio) {
  if (ratio < 1.0f) {
    QMessageBox::warning(this, "Warning", "Invalid ratio value. The value must be greater than or equal to 1");
    return;
  }

  RecreateGraphicsScene();

  QGraphicsPixmapItem *pixmapItem = new QGraphicsPixmapItem(QPixmap::fromImage(image));
  pixmapItem->setScale(ratio);

  mGraphicsScene->addItem(pixmapItem);

  auto pixmapSize = pixmapItem->pixmap().size();
  auto origSize = image.size();
  auto diffSize = QSizeF(origSize.width() / pixmapSize.width(), origSize.height() / pixmapSize.height());
  pixmapItem->setTransform(QTransform::fromScale(diffSize.width(), diffSize.height()));

  UpdateSizeLabel(image);
}

void MainWindow::UpdateImageComboBox(const ImageData &imageData) {
  for (int i = 0; i < mComboImage->count(); ++i) {
    QVariant variant = mComboImage->itemData(i);
    QUuid uuid = variant.toUuid();
    if (uuid == imageData.Uuid) {
      mComboImage->setCurrentIndex(i);
      return;
    }
  }
}

void MainWindow::UpdateLayerComboBox(const ImageData &imageData) {
  mComboLayer->clear();
  for (unsigned int i = 0; i < imageData.GetMaxDownscaleCount(); ++i) {
    mComboLayer->addItem(QString::number(i));
  }
  mComboLayer->setCurrentIndex(0);
}

void MainWindow::UpdateSizeLabel(const QImage &image) {
  QString text = "Size: ";
  text.append(QString::number(image.width()));
  text.append("x");
  text.append(QString::number(image.height()));
  mLabelSize->setText(text);
}

void MainWindow::ClearGraphicsScene() {
  for (auto &item : mGraphicsScene->items()) {
    mGraphicsScene->removeItem(item);
  }
}

void MainWindow::RecreateGraphicsScene() {
  // TODO: Fix ugly workaround
  ClearGraphicsScene();
  mGraphicsScene = std::make_unique<QGraphicsScene>(this);
  mImageView->setScene(mGraphicsScene.get());
}

}  // namespace Movavi