#pragma once
#include <memory>
#include <vector>
#include <iostream>

// Qt Includes
// Core things
#include <QApplication>
#include <QWindow>
#include <QtWidgets>

// Structs and Utils
#include <QtAlgorithms>
#include <QUuid>
#include <QDir>
#include <QFile>
#include <QStandardPaths>