#include <Movavi/ImageData.hpp>
#include <Movavi/ImageScaler.hpp>

namespace Movavi {

ImageData::ImageData(const QFileInfo& fileInfo)
    : Uuid(QUuid::createUuid()), FileInfo(fileInfo) {
  mBaseImage = QImage(fileInfo.filePath());
  mMaxDownscaleCount = int(std::ceilf(std::log2f(std::min(mBaseImage.width(), mBaseImage.height()))));
  mBaseGraphicsItem = std::make_shared<QGraphicsPixmapItem>(QPixmap::fromImage(mBaseImage));
  mGraphicsItems.resize(mMaxDownscaleCount);
  for (auto& item : mGraphicsItems) {
    item = nullptr;
  }
  mGraphicsItems[0] = mBaseGraphicsItem;
}

ImageData::ImageData(ImageData&& other) noexcept
    : mGraphicsItems(std::move(other.mGraphicsItems)),
      mMaxDownscaleCount(other.mMaxDownscaleCount),
      Uuid(std::move(other.Uuid)),
      FileInfo(std::move(other.FileInfo)),
      mBaseImage(std::move(other.mBaseImage)) {
  mGraphicsItems.resize(mMaxDownscaleCount);
}

std::shared_ptr<QGraphicsPixmapItem> ImageData::GetGraphicsItem(unsigned int level) const {
  if (level < 0 && level >= mMaxDownscaleCount) {
    return nullptr;
  }
  if (mGraphicsItems[level]) {
    return mGraphicsItems[level];
  }
  return nullptr;
}

std::shared_ptr<QGraphicsPixmapItem> ImageData::CreateDownscaleImage(unsigned int level) {
  auto existingObject = GetGraphicsItem(level);
  if (existingObject) {
    return existingObject;
  }

  std::shared_ptr<QGraphicsPixmapItem> prevLevel = std::dynamic_pointer_cast<QGraphicsPixmapItem>(CreateDownscaleImage(level - 1));
  std::shared_ptr<QGraphicsPixmapItem> curLevel = std::make_shared<QGraphicsPixmapItem>(prevLevel->pixmap());
  mGraphicsItems[level] = curLevel;

  ImageScaler scaler(curLevel->pixmap().toImage());
  scaler.Downscale(2.0f);
  curLevel->setPixmap(QPixmap::fromImage(scaler.GetImage()));
  return curLevel;
}

unsigned int ImageData::GetMaxDownscaleCount() const {
  return mMaxDownscaleCount;
}

const QImage& ImageData::GetBaseImage() const {
  return mBaseImage;
}

bool ImageData::operator==(const ImageData& other) const {
  return Uuid == other.Uuid;
}

bool ImageData::operator!=(const ImageData& other) const {
  return !(*this == other);
}

}  // namespace Movavi