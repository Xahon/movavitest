
#include <Movavi/MainWindow.hpp>

int main(int argc, char *argv[]) {
  QApplication app(argc, argv);
  Movavi::MainWindow w;
  w.show();
  w.setMinimumSize(600, 600);
  w.setSizePolicy(QSizePolicy::Policy::MinimumExpanding, QSizePolicy::Policy::MinimumExpanding);
  return app.exec();
}
