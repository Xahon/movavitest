#pragma once

class IShape {
 public:
  virtual ~IShape() = default;
  virtual double Area() const = 0;
};

class Rectangle : public IShape {
  float width, height;

 public:
  Rectangle(float width, float height)
      : width(width), height(height) {
  }

  double Area() const override {
    return width * height;
  }
};

struct Point {
  int x, y;
};

class Triangle : public IShape {
  Point a, b, c;

 public:
  double Area() const override {
    return abs((a.x * (b.y - c.y) + b.x * (c.y - a.y) + c.x * (a.y - b.y)) / 2.0f);
  }
};

class Circle : public IShape {
  float radius;

 public:
  Circle(float radius)
      : radius(radius) {
  }

  double Area() const override {
    return 3.141595653589793238 * radius * radius;
  }
};