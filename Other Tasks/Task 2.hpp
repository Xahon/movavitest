#pragma once
#include <cstdio>

void Example1() {
  FILE *f = fopen("name", "w");
  try {
    // ��� ���, ��� ������� ����� ���� ��������� ����������
  } catch (...) {
    // ��� ���, ������� ������������ ����������
  } finally {
    fclose(f);
  }
}

class FileWrapper final {
  FILE *f;

 public:
  FileWrapper(const char* name, const char* flags) {
    f = fopen(name, flags);
  }

  ~FileWrapper() {
    if (f) {
      fclose(f);
      f = nullptr;
    }
  }

  const FILE* GetFile() const {
    return f;
  }
};

void Example2() {
  FileWrapper f("name", "w");
  // ��� ���, ��� ������� ����� ���� ��������� ����������
}